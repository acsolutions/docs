- [Содержание](../README.md#Содержание)

# Получение информации по истории действий лида.
`/api/v2/orders/public/{api_key}/get_bpm_actions`

> [Область видимости](authorization.md): **организация**

# Описание:
При обращении можно будет увидеть историю переходов которые произошли с [лидом](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4) внутри кампании

Лимит 10 ключей за один запрос.

Для получения [статусов](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%81%D1%82%D0%B0%D1%82%D1%83%D1%81) необходимо отправить на ``` /api/v2/orders/public/{api_key}/get_bpm_actions ``` методом GET следующие параметры.

| Переменная  | Значение | Необходимость |
| ------ | ------ | ------ |
| keys | Ключи лидов, перечисленные через запятую. | Обязательный |

Параметры ответа:

| Переменная  | Значение |
| ------ | ------ |
| id | ID Действия |
| type | Тип действия |
| title | Наименование действия |
| run_time | Время начала действия |
| end_time | Время завершения действия |
| jump_id | ID Перехода, по которому перешел лид при завершении действия |
| jump_title | Наименование перехода |
| next_action_id | Следующее действие|

Пример запроса
``` /api/v2/orders/public/XXXXXXXXXX/get_bpm_actions?keys=XXXXX,YYYYY,LLLLL ```

Пример ответа:
*Порядок действий в ответе, отсортирован от крайнего к стартовому.*
```
{
   "LLLLL": [],
   "XXXXX": [
       {
           "id": 451,  
           "type": "segment",  
           "title": "Подтверждён",  
           "run_time": "2022-02-10 15:18:42",
           "end_time": "2022-02-10 15:19:09",
           "jump_id": null,  
           "jump_title": null,
           "next_action_id": null
       },
       {
           "id": 307,
           "type": "bot",
           "title": "Подтверждение заказа. 2",
           "run_time": "2022-02-10 15:10:59",
           "end_time": "2022-02-10 15:16:11",
           "jump_id": 484,
           "jump_title": "Подтвержден 4",
           "next_action_id": 298
       },
       {
           "id": 300,
           "type": "bot",
           "title": "Продажа с улучшенным предложением",
           "run_time": "2022-02-10 14:55:56",
           "end_time": "2022-02-10 15:04:31",
           "jump_id": 473,
           "jump_title": "Заинтересован 2",
           "next_action_id": 307
       },
       {
           "id": 299,
           "type": "bot",
           "title": "Выявление заинтересованности",
           "run_time": "2022-02-10 14:32:18",
           "end_time": "2022-02-10 14:54:56",
           "jump_id": 472,
           "jump_title": "Отказ 1",
           "next_action_id": 300
       }
   ],
   "YYYYY": [
       {
           "id": 335,
           "type": "segment",
           "title": "Отказ",
           "run_time": "2022-02-25 09:30:45",
           "end_time": "2022-02-25 09:47:54",
           "jump_id": null,
           "jump_title": null,
           "next_action_id": null
       },
       {
           "id": 333,
           "type": "bot",
           "title": "Распределение",
           "run_time": "2022-02-25 09:00:58",
           "end_time": "2022-02-25 09:30:44",
           "jump_id": 522,
           "jump_title": "TEST 111",
           "next_action_id": 335
       }
   ]
}
```
