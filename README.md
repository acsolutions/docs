# Содержание

+ [Определения в документации](docs/definitions.md)
+ [Авторизация](docs/authorization.md)
+ [Кампания](docs/definitions.md#кампания) `/api/v2/bpm/public/...`
  * [Добавление лидов в кампанию](docs/add_orders.md) `bp/{api_key}/add_orders`
  * [Создание файла загрузки в кампании](docs/create_load.md) `bp/{api_key}/create_load`
+ [Лиды](definitions.md#лид) `/api/v2/orders/public/...`
  * [Установка статуса лидам](docs/set_status.md) `{api_key}/set_status`
  * [Прерывание обработки лида](docs/trash_order.md) `{api_key}/trash_order`
  * [Получение статусов лидов](docs/get_status.md) `{api_key}/get_status`
  * [Получение информации по истории действий лида](docs/get_bpm_actions.md) `{api_key}/get_bpm_actions`
  * [Получение информации по звонкам лида](docs/get_calls.md) `{api_key}/get_calls`
  * [Получение информации по текстовым сообщениям лида](docs/get_messages.md) `{api_key}/get_messages`
  * [Получение транскрибации лида](docs/get_dialogs.md) `{api_key}/get_dialogs`

# Домен для выполнения запросов

`https://back.crm.acsolutions.ai`

# Необходимые заголовки

| Заголовок | Значение     |
| ------------------ | -------------------- |
| `content-type`   | `application/json` |
| `accept`         | `application/json` |

# Примеры запросов

## CURL

### Добавление лида

```sh
curl --request POST \
  --url https://back.crm.acsolutions.ai/api/v2/bpm/public/bp/CAMPAIGN_API_KEY/add_orders \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json' \
  --data '[
	{
		"phone": "XXXXXXXXXXX",
		"info": {
			"text": "Произвольный текст"
		}
	}
]'
```

### Получение статусов лида

```sh
curl --request GET \
  --url 'https://back.crm.acsolutions.ai/api/v2/orders/public/ORGANIZATION_API_KEY/get_status?keys=XXXXXXX' \
  --header 'Accept: application/json' \
  --header 'Content-Type: application/json'
```
