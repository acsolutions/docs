- [Содержание](../README.md#Содержание)

# Получение информации по звонкам лида.
`/api/v2/orders/public/{api_key}/get_calls`

> [Область видимости](authorization.md): **организация**

## Описание:
Метод возвращает информацию по [звонкам](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%B7%D0%B2%D0%BE%D0%BD%D0%BA%D0%B8) которые привязаны к [лиду](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4).

Лимит 10 ключей за один запрос.

Для получения звонков необходимо отправить на ``` /api/v2/orders/public/{api_key}/get_calls ``` методом GET следующие параметры.

| Переменная  | Значение | Необходимость |
| ------ | ------ | ------ |
| keys | Ключи лидов, перечисленные через запятую. | Обязательный |
| from | С какой даты получить данные. Формат: Y-m-d | Необязательный |
| to | До какой даты получить данные. Формат: Y-m-d | Необязательный |

Параметры ответа:

| Переменная  | Значение |
| ------ | ------ |
| order_key | Ключ лида |
| link | Ссылка на запись разговора |
| duration | Продолжительность звонка|
| time | Дата звонка |
| type | Тип звонка: *auto - Автодозвон оператором; bot - Автодозвон роботом; in - Входящий к оператору; bot_incall - Входящий к роботу; out - Исходящий оператором* |

Пример запроса
``` /api/v2/orders/public/XXXXXXXXXX/get_calls?keys=XXXXX,YYYYY ```

Пример ответа
```
[
   {
       "order_key": "XXXXX",
       "link": "https://rec.dstcrm.online/2019/06/25/123456789.123456.mp3",
       "duration": null,
       "time": "2019-06-25 17:58:45",
       "type": "auto"
   },
   {
       "order_key": "XXXXX",
       "link": "https://rec.dstcrm.online/2019/06/25/987654321.654321.mp3",
       "duration": null,
       "time": "2019-06-25 18:17:30",
       "type": "auto"
   },
   {
       "order_key": "YYYYY",
       "link": "https://rec.dstcrm.online/2019/06/25/1122334455.456789.mp3",
       "duration": null,
       "time": "2019-06-25 18:50:52",
       "type": "auto"
   }
]
```
