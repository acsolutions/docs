- [Содержание](../README.md#Содержание)

# Получение статусов лидов
`/api/v2/orders/public/{api_key}/get_status`

> [Область видимости](authorization.md): **организация**

## Описание:
Данный метод api позволяет получать текущие [статусы](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%81%D1%82%D0%B0%D1%82%D1%83%D1%81) [лида](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4) по ключам заказов

Лимит 10 лидов за один запрос.

Для получения статусов необходимо отправить на ``` /api/v2/orders/public/{api_key}/get_status ``` методом GET следующие параметры.

| Переменная  | Значение | Необходимость |
| ------ | ------ | ------ |
| keys | Ключи лидов, перечисленные через запятую. | Обязательный |
| lang | Локализация названий статусов. По умолчанию ru. | Необязательный|

Параметры ответа:

| Переменная  | Значение |
| ------ | ------ |
| status_group_5 | Группа статуса |
| status_id | ID статуса |
| name | Наименование статуса |

Пример запроса
``` /api/v2/orders/public/XXXXXXXXXX/get_status?keys=XXXXX,YYYYY ```

Пример ответа:
```
{
   "XXXXX": {
       "status_group_5": {
           "status_id": 10, // ID статуса
           "name": "на проверке" // Наименование статуса
       },
       "status_group_1": {
           "status_id": 11,
           "name": "подтвержден"
       }
   },
   "YYYYY": {      
       "status_group_1": {
           "status_id": 12,
           "name": "ожидает"
       }
   }
}
 ```
