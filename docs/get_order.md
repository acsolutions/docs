- [Содержание](../README.md#Содержание)

# Получение полной информации по лиду.
`/api/v2/orders/public/{api_key}/get_order`

> [Область видимости](authorization.md): **кампания**

# Описание:
При обращении можно будет текущее состояние [лида](definitions.md#%D0%BB%D0%B8%D0%B4) со статусами и положением в БП

**api_key** - это ключ из API на странице кампании.

Для получения информации необходимо отправить на `/api/v2/orders/public/{api_key}/get_order` методом GET следующие параметры.

| Переменная  | Значение | Необходимость |
| ---------- | ----------------------------------- | --------------------------------------- |
| import_id  | ID контакта в системе заказчика     | Необязательный при указании key         |
| key        | Ключ лида                           | Необязательный при указании import_id   |

Пример запроса:  
`/api/v2/orders/public/XXXXXXXXXX/get_order?import_id=CC-154427`

Пример ответа:
```json
{
    "order": {
        "key": "478a6b4c",
        "import_id": "CC-154427",
        "phone": 79110000008,
        "full_name": "John Doe",
        "info": {
            "third_party_phone": 78880000008,
            "third_party_name": "Jane Doe",
            "debt_summ": 42000,
            "partial_repayment": 4200 ,
            "payment_delay": 12
        }
    },
    "statuses": {
        "status_group_5": {
            "status_id": 10,
            "name": "на проверке"
        },
        "status_group_1": {
            "status_id": 11,
            "name": "подтвержден"
        }
    },
    "bpm_action": {
        "id": 298,  
        "title": "Перевод в другой БП 1",  
        "run_time": "2022-02-10 15:18:42",
        "end_time": null,
        "jump_id": null,  
        "jump_title": null,
        "next_action_id": null
    }
}
```
