- [Содержание](../README.md#Содержание)

# Получение информации по текстовым сообщениям лида.
`/api/v2/orders/public/{api_key}/get_messages`

> [Область видимости](authorization.md): **организация**

## Описание:
Метод возвращает [текстовые сообщения](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D1%82%D0%B5%D0%BA%D1%81%D1%82%D0%BE%D0%B2%D1%8B%D0%B5-%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D1%8F) которые привязаны к [лиду](https://gitlab.com/acsolutions/docs/-/blob/main/docs/definitions.md#%D0%BB%D0%B8%D0%B4).

Лимит 10 ключей за один запрос.

Для получения текстовых сообщений необходимо отправить на ``` /api/v2/orders/public/{api_key}/get_messages ``` методом GET следующие параметры.

| Переменная  | Значение | Необходимость |
| ------ | ------ | ------ |
| keys | Ключи лидов, перечисленные через запятую.   | Необязательный при указании import_ids |
| import_ids  | ID контактов в системе заказчика     | Необязательный при указании keys       |
| from | С какой даты получить данные. Формат: Y-m-d | Необязательный |
| to | До какой даты получить данные. Формат: Y-m-d  | Необязательный |

Параметры ответа:

| Переменная  | Значение |
| ------ | ------ |
| order_key | Ключ лида |
| type | Тип сообщения: *sms - СМС сообщение; chatbot - сообщение чатбота* |
| status | Статус СМС-сообщения |
| author | Отправитель: *bot - Автоматическая отправка; user - Ручная отправка пользователем* |
| text | Текст сообщения |
| created_at | Дата отправки |
| action_id | ID Действия, если есть |
| action_title | Наименование действия, если есть. |
| bpm_bp_key | Ключ бизнес-процесса, если есть |
| bpm_bp_title | Наименование бизнес-процесса, если есть|

Статусы СМС:

| Статус  | Описание |
| ------ | ------ |
| `ok`          | Сообщение доставлено |
| `send`        | Принято смс-центром |
| `not_sended`  | Ошибка в момент отправки |
| `not_delivered`  | Не доставлено смс-центром |
| `no_balance`  | Недостаточно баланса |
| `error`       | Не доставлено из-за ошибки смс-центром |
| `wait`        | Принято смс-центром, но пока не доставлено |
| `final-error` | Ошибка. Смс не принято смс-центром |

Пример запроса
``` /api/v2/orders/public/XXXXXXXXXX/get_messages?keys=XXXXX,YYYYY ```

Пример ответа
```
[
   {
       "order_key": "XXXXX",
       "type": "sms",
       "status": "send",
       "author": "bot",
       "text": "Uvadjaemyi pokupatel'! Posylka pribyla, nahoditsya v kur'erskoi sludjbe.",
       "created_at": "2021-02-17 15:04:39",
       "action_id": null,
       "action_title": null,
       "bpm_bp_key": null,
       "bpm_bp_title": null
   },
   {
       "order_key": "XXXXX",
       "type": "sms",
       "status": "ok",
       "author": "user",
       "text": "Dobryi den'! Adres el.pochty client@mail.ru po voprosam vozvrata i obmena",
       "created_at": "2021-05-05 15:03:06",
       "action_id": null,
       "action_title": null,
       "bpm_bp_key": null,
       "bpm_bp_title": null
   },
   {
       "order_key": "YYYYY",
       "type": "chatbot",
       "author": "bot",
       "text": "1",
       "created_at": "2022-05-03 14:51:17",
       "action_id": 1246,
       "action_title": "Чат-бот",
       "bpm_bp_key": "7dfb54b68d",
       "bpm_bp_title": "test 05-03"
   }
]
```
