- [Содержание](../README.md#Содержание)

# Сервис транскрибации диалога лида
`/api/v2/orders/public/{api_key}/get_dialogs`

> [Область видимости](authorization.md): **кампания**

Данный метод api позволяет получать транскрибацию диалога лида.
Для получения транскрибации необходимо отправить на ``` /api/v2/orders/public/{api_key}/get_dialogs``` методом GET следующие параметры.

### Параметры запроса

| Поле         | Описание                                  | Обязательное                 |
| :----------- | :---------------------------------------- | :--------------------------- |
| `keys`       | ключи лидов, перечисленные через запятую  | нет, при указании import_ids |
| `import_ids` | импорт ID, перечисленные через запятую    | нет, при указании keys       |
| `from`       | начальная дата в формате: "Y-m-d H:i:s"   | нет                          |
| `to`         | конечная дата в формате: "Y-m-d H:i:s"    | нет                          |
| `type`       | Тип сущности, доступные значения : "call" | да                           |
| `call`       | транскрибация звонков                     |                              |
| `api_key`    | API ключ                                  |                              |

### Параметры ответа

| Поле                      | Описание                  |
| :------------------------ | :------------------------ |
| `key`                     | ключ лида                 |
| `import_id`               | импорт ID лида            |
| `dialogs`                 | массив диалогов           |
| `dialogs.*.*.client_text` | текст клиента             |
| `dialogs.*.*.robot_text`  | текст бота                |
| `dialogs.*.*.created_at`  | дата фиксирования диалога |

### Пример запроса

/api/v2/orders/public/{api_key}/dialogs?keys=xxxxxxxx,yyyyyyyy&type=call

### Пример ответов

Успешный:

```json
{
    "key": "xxxxxxxx",
    "import_id": "",
    "dialogs": []
},
{
    "key": "yyyyyyyy",
    "import_id": "",
    "dialogs": [
    [
        {
            "client_text": "\/start",
            "robot_text": "Алло, Здравствуйте",
            "created_at": "2024-12-20 08:19:02"
        },
        {
            "client_text": "Алло да",
            "robot_text": "Звоню вам сообщить",
            "created_at": "2024-12-20 08:19:04"
        }
    ],
    [
        {
            "client_text": "\/start",
            "robot_text": "Алло, Здравствуйте 2",
            "created_at": "2024-12-21 10:25:12"
        },
        {
            "client_text": "Алло да 2",
            "robot_text": "Звоню вам сообщить 2",
            "created_at": "2024-12-21 10:25:14"
        }
    ]
    ]
}

```
Ошибка доступов:

```json
{
	"message": "Permission denied",
}

```
Ошибка валидации:

```json
{
	"message": "The given data was invalid.",
	"errors": {
		"type": [
			"The selected type is invalid."
		]
	}
}

```